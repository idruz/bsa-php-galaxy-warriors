<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Builder\ShipBuilder;

class ExecutorShip implements ShipBuilder
{
    private $ship;

    public function __construct()
    {
        $this->ship = new Ship();
    }

    public function setName()
    {
        $this->ship->name = 'executor';
    }
    public function setFullname()
    {
        $this->ship->fullname = 'Executor';
    }
    public function setStats()
    {
        $this->ship->stats =  [
            'strength' => 10,
            'armor' => 10,
            'luck' => 10,
            'health' => 100,
            'hold' => ['🔋', '🔮', '🔮']
        ];
    }
    public function make(): Ship
    {
        return $this->ship;
    }
}