<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Builder\ShipBuilder;

class PlayerShip implements ShipBuilder
{
    private $ship;

    public function __construct()
    {
        $this->ship = new Ship();
    }

    public function setName()
    {
        $this->ship->name = 'player';
    }
    public function setFullname()
    {
        $this->ship->fullname = 'Player Spaceship';
    }
    public function setStats()
    {
        $this->ship->stats =  [
            'strength' => 5,
            'armor' => 5,
            'luck' => 5,
            'health' => 100,
            'hold' => ['_', '_', '_'],
        ];
    }
    public function make(): Ship
    {
        return $this->ship;
    }
}