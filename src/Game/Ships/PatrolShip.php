<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Builder\ShipBuilder;

class PatrolShip implements ShipBuilder
{
    private $ship;

    public function __construct()
    {
        $this->ship = new Ship();
    }

    public function setName()
    {
        $this->ship->name = 'patrol';
    }
    public function setFullname()
    {
        $this->ship->fullname = 'Patrol Spaceship';
    }
    public function setStats()
    {
        $this->ship->stats =  [
            'strength' => rand(3, 4),
            'armor' => rand(2, 4),
            'luck' => rand(1, 2),
            'health' => 100,
            'hold' => ['🔋']
        ];
    }
    public function make(): Ship
    {
        return $this->ship;
    }
}