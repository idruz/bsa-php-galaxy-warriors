<?php

namespace BinaryStudioAcademy\Game\Ships;

use BinaryStudioAcademy\Game\Contracts\Builder\ShipBuilder;

class BattleShip implements ShipBuilder
{
    private $ship;

    public function __construct()
    {
        $this->ship = new Ship();
    }

    public function setName()
    {
        $this->ship->name = 'battle';
    }
    public function setFullname()
    {
        $this->ship->fullname = 'Battle Spaceship';
    }
    public function setStats()
    {
        $this->ship->stats =  [
            'strength' => rand(5, 8),
            'armor' => rand(6, 8),
            'luck' => rand(3, 6),
            'health' => 100,
            'hold' => ['🔋', '🔮']
        ];
    }
    public function make(): Ship
    {
        return $this->ship;
    }
}