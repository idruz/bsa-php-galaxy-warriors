<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class SpiralGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'spiral';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Spiral';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'patrol';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}