<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class IsopGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'isop';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Isop';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'executor';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}