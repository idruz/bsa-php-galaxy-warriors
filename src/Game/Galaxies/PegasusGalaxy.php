<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class PegasusGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'pegasus';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Pegasus';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'patrol';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}