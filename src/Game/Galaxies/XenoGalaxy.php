<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class XenoGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'xeno';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Xeno';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'battle';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}