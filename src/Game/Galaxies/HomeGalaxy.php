<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class HomeGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'home';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Home Galaxy';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'player';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}