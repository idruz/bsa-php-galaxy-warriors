<?php

namespace BinaryStudioAcademy\Game\Galaxies;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class ShiarGalaxy implements GalaxyBuilder
{
    private $galaxy;

    public function __construct()
    {
        $this->galaxy = new Galaxy();
    }

    public function setName()
    {
        $this->galaxy->name = 'shiar';
    }
    public function setFullname()
    {
        $this->galaxy->fullname = 'Shiar';
    }
    public function setAvailableShip()
    {
        $this->galaxy->availableShip = 'battle';
    }
    public function make(): Galaxy
    {
        return $this->galaxy;
    }
}