<?php

namespace BinaryStudioAcademy\Game\Contracts\Builder;

use BinaryStudioAcademy\Game\Contracts\Builder\GalaxyBuilder;

class GalaxyDirector
{
    public function build(GalaxyBuilder $builder)
    {
        $builder->setName();
        $builder->setFullname();
        $builder->setAvailableShip();

        return $builder->make();
    }
}
