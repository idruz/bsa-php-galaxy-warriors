<?php

namespace BinaryStudioAcademy\Game\Contracts\Builder;

interface ShipBuilder
{
    public function setName();
    public function setFullname();
    public function setStats();
    public function make();
}
