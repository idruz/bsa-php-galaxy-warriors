<?php

namespace BinaryStudioAcademy\Game\Contracts\Builder;

use BinaryStudioAcademy\Game\Contracts\Builder\ShipBuilder;

class ShipDirector
{
    public function build(ShipBuilder $builder)
    {
        $builder->setName();
        $builder->setFullname();
        $builder->setStats();

        return $builder->make();
    }
}
