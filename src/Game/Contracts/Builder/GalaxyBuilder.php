<?php

namespace BinaryStudioAcademy\Game\Contracts\Builder;

interface GalaxyBuilder
{
    public function setName();
    public function setFullname();
    public function setAvailableShip();
    public function make();
}
