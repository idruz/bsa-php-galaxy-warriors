<?php

namespace BinaryStudioAcademy\Game;

class DataStorage
{
    public static $instances = [];

    protected function __construct() { }

    protected function __clone() { }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    public static function getInstance(): array
    {
        if (empty(static::$instances)) {
            static::$instances = [
                'player' => [
                    'current_galaxy' => 'home',
                    'ship' => [
                        'strength' => 5,
                        'armor' => 5,
                        'luck' => 5,
                        'health' => 100,
                        'hold' => ['_', '_', '_'],
                    ],
                ],
                'galaxy' => [],
                'galaxies' => [
                    'home' => [
                        'galaxy' => 'Home Galaxy',
                        'spaceship' => 'player'
                    ],
                    'andromeda' => [
                        'galaxy' => 'Andromeda',
                        'spaceship' => 'patrol'
                    ],
                    'pegasus' => [
                        'galaxy' => 'Pegasus',
                        'spaceship' => 'patrol'
                    ],
                    'spiral' => [
                        'galaxy' => 'Spiral',
                        'spaceship' => 'patrol'
                    ],
                    'shiar' => [
                        'galaxy' => 'Shiar',
                        'spaceship' => 'battle'
                    ],
                    'xeno' => [
                        'galaxy' => 'Xeno',
                        'spaceship' => 'battle'
                    ],
                    'isop' => [
                        'galaxy' => 'Isop',
                        'spaceship' => 'executor'
                    ]
                ],
                'ship' => [
                    'patrol' => [
                        'name' => 'Patrol Spaceship',
                        'stats' => [
                            'strength' => 4,
                            'armor' => 4,
                            'luck' => 2,
                            'health' => 100,
                        ]
                    ],
                    'battle' => [
                        'name' => 'Battle Spaceship',
                        'stats' => [
                            'strength' => 8,
                            'armor' => 7,
                            'luck' => 6,
                            'health' => 100,
                        ]
                    ],
                    'executor' => [
                        'name' => 'Executor',
                        'stats' => [
                            'strength' => 10,
                            'armor' => 10,
                            'luck' => 10,
                            'health' => 100,
                        ]
                    ],
                ],
            ];
        }

        return static::$instances;
    }
}