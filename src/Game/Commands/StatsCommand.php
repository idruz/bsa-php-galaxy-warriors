<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\DataStorage;
use BinaryStudioAcademy\Game\Contracts\Commands\Command;

class StatsCommand implements Command
{
    public function execute(): string
    {
        $dataStorage = DataStorage::$instances['player']['ship'];

        $hold = '[ _ _ _ ]';
        if (
            !empty($dataStorage['hold'])
            and is_array($dataStorage['hold'])
        ) {
            $newDataToHold = '[ ';
            foreach ($dataStorage['hold'] as $data) {
                $newDataToHold .= $data . ' ';
            }
            $newDataToHold .= ']';

            $hold = $newDataToHold;
        }

        return 'Spaceship stats:' . PHP_EOL
            . 'strength: ' . ($dataStorage['strength'] ?: 5) . PHP_EOL
            . 'armor: ' . ($dataStorage['armor'] ?: 5) . PHP_EOL
            . 'luck: ' . ($dataStorage['luck'] ?: 5)  . PHP_EOL
            . 'health: ' . ($dataStorage['health'] ?: 100)   . PHP_EOL
            . 'hold: ' . ($hold) . PHP_EOL;
    }
}