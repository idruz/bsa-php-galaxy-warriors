<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;

class ExitCommand implements Command
{
    public function execute(): string
    {
        exit(PHP_EOL . 'Game exit :( Goodbye!' . PHP_EOL);
    }
}