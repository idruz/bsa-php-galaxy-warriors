<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\DataStorage;
use BinaryStudioAcademyTests\Game\Messages;

class RestartCommand implements Command
{
    public function execute(): string
    {
        DataStorage::$instances['player']['current_galaxy'] = 'home';
        DataStorage::$instances['player']['ship'] = [
            'strength' => 5,
            'armor' => 5,
            'luck' => 5,
            'health' => 100,
            'hold' => ['_', '_', '_'],
        ];

        return Messages::homeGalaxy();
    }
}