<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;

class ApplyReactorCommand implements Command
{
    public function execute(): string
    {
        return 'apply magnet reactor 🔋 to increase spaceship health level on 20 points';
    }
}