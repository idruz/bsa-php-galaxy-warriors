<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\DataStorage;

class WhereAmICommand implements Command
{
    private $galaxyName;

    public function __construct()
    {
        $this->galaxyName = DataStorage::$instances['player']['current_galaxy'];
    }

    public function execute(): string
    {
        $galaxy = DataStorage::$instances['galaxies'][$this->galaxyName];

        return "Galaxy: {$galaxy['galaxy']}" . PHP_EOL;
    }
}