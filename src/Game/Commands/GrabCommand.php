<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\DataStorage;
use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Stats;
use BinaryStudioAcademyTests\Game\Messages;

class GrabCommand implements Command
{
    public function execute(): string
    {
        $dataStorage = DataStorage::$instances;

        if ($dataStorage['player']['current_galaxy'] == 'home') {
            return Messages::errors('home_galaxy_grab');
        } elseif (
            $dataStorage['player']['current_galaxy'] !== 'home'
            and $dataStorage['player']['ship']['health'] > 0
            and $dataStorage['galaxy']['ship']['stats']['health'] > 0
        ) {
            return Messages::errors('grab_undestroyed_spaceship');
        } elseif (
            $dataStorage['player']['ship']['health'] > 0
            and $dataStorage['galaxy']['ship']['stats']['health'] <= 0
        ) {
            var_dump('Oponent ship destroy!!! You will grab resources');
        }

        return '';
    }
}