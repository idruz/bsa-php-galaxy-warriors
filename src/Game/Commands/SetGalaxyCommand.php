<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\DataStorage;
use BinaryStudioAcademyTests\Game\Messages;
use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Contracts\Builder\{GalaxyDirector,ShipDirector};

class SetGalaxyCommand implements Command
{
    private $galaxyName;

    public function __construct($name = null)
    {
        $this->galaxyName = $name;
    }

    public function execute(): string
    {
        if (empty($this->galaxyName)) {
            return 'Please select a galaxy! Enter the correct command, for example: set-galaxy home';
        }

        $galaxyDirector = new GalaxyDirector();
        $galaxyClassName = "BinaryStudioAcademy\\Game\\Galaxies\\" . ucfirst($this->galaxyName) . 'Galaxy';
        if (!class_exists($galaxyClassName)) {
            /**
             * Todo
             * create custom exceptions
             */
            return Messages::errors('undefined_galaxy');
        }

        $galaxy = new $galaxyClassName();
        $galaxyBuild = $galaxyDirector->build($galaxy);

        $shipDirector = new ShipDirector();
        $shipClassName = "BinaryStudioAcademy\\Game\\Ships\\" . ucfirst($galaxyBuild->availableShip) . 'Ship';
        if (!class_exists($shipClassName)) {
            /**
             * Todo
             * create custom exceptions
             */
            return 'Nah. No specified ship found.';
        }

        $patrolShip     = new $shipClassName();
        $shipBulid = $shipDirector->build($patrolShip);

        DataStorage::$instances['player']['current_galaxy'] = $this->galaxyName;

        if ($this->galaxyName !== 'home') {
            DataStorage::$instances['galaxy']['name'] = $galaxyBuild->fullname;
            DataStorage::$instances['galaxy']['ship']['name'] = $shipBulid->fullname;
            DataStorage::$instances['galaxy']['ship']['stats'] = $shipBulid->stats;
            $galaxyData = DataStorage::$instances['galaxy'];

            return "Galaxy: {$galaxyData['name']}." . PHP_EOL
                . "You see a {$galaxyData['ship']['name']}: " . PHP_EOL
                . 'strength: ' . $galaxyData['ship']['stats']['strength'] . PHP_EOL
                . 'armor: ' . $galaxyData['ship']['stats']['armor'] . PHP_EOL
                . 'luck: ' . $galaxyData['ship']['stats']['luck']  . PHP_EOL
                . 'health: ' . $galaxyData['ship']['stats']['health']   . PHP_EOL;
        } elseif ($this->galaxyName == 'home') {
            return 'Galaxy: Home Galaxy.' . PHP_EOL;
        }

        return Messages::errors('undefined_galaxy');
    }
}