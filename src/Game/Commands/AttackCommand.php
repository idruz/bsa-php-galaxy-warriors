<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\DataStorage;
use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademy\Game\Helpers\Math;
use BinaryStudioAcademy\Game\Helpers\Random;
use BinaryStudioAcademyTests\Game\Messages;

class AttackCommand implements Command
{
    public function execute(): string
    {
        $dataStorage = DataStorage::$instances;
        if ($dataStorage['player']['current_galaxy'] == 'home') {
            return Messages::errors('home_galaxy_attack');
        } elseif (
            $dataStorage['player']['current_galaxy'] !== 'home'
            and $dataStorage['player']['ship']['health'] > 0
            and $dataStorage['galaxy']['ship']['stats']['health'] > 0
        ) {
            $playerData = $dataStorage['player']['ship'];
            $oponentData = $dataStorage['galaxy']['ship'];

            $mathData = new Math();
            $random = new Random();

            $playerDealtDamage = $mathData->luck($random, $playerData['luck']);
            $playerDealtDamageValue = 0;
            if ($playerDealtDamage) {
                $playerDealtDamageValue = $mathData->damage($playerData['strength'], $playerData['armor']);
                DataStorage::$instances['galaxy']['ship']['stats']['health'] -= $playerDealtDamageValue;

                if (DataStorage::$instances['galaxy']['ship']['stats']['health'] <= 0) {
                    return Messages::destroyed($dataStorage['galaxy']['ship']['name']);
                }
            }

            $oponentDealtDamage = $mathData->luck($random, $oponentData['stats']['luck']);
            $oponentDealtDamageValue = 0;
            if ($oponentDealtDamage) {
                $oponentDealtDamageValue = $mathData->damage($oponentData['stats']['strength'], $oponentData['stats']['armor']);
                DataStorage::$instances['player']['ship']['health'] -= $oponentDealtDamageValue;
                if (DataStorage::$instances['player']['ship']['health'] <= 0) {
                    DataStorage::$instances['player']['current_galaxy'] = 'home';
                    DataStorage::$instances['player']['ship'] = [
                        'strength' => 5,
                        'armor' => 5,
                        'luck' => 5,
                        'health' => 100,
                        'hold' => ['_', '_', '_'],
                    ];
                    return Messages::die();
                }
            }

            return Messages::attack(
                $dataStorage['galaxy']['ship']['name'],
                $playerDealtDamageValue,
                DataStorage::$instances['galaxy']['ship']['stats']['health'],
                $oponentDealtDamageValue,
                DataStorage::$instances['player']['ship']['health']
            );
        } elseif($dataStorage['player']['current_galaxy'] !== 'home') {
            if (DataStorage::$instances['player']['ship']['health'] <= 0) {
                return Messages::die();
            }

            if (
                DataStorage::$instances['galaxy']['ship']['stats']['health'] <= 0
                and !empty(DataStorage::$instances['galaxy']['ship']['stats']['hold'])
            ) {
                return Messages::destroyed($dataStorage['galaxy']['ship']['name']);
            } elseif (
                DataStorage::$instances['galaxy']['ship']['stats']['health'] <= 0
                and empty(DataStorage::$instances['galaxy']['ship']['stats']['hold'])
            ) {
                return 'There is nothing to grab. Go to Home Galaxy or wherever you want';
            }
        }

        return '';
    }
}