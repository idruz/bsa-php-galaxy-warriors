<?php

namespace BinaryStudioAcademy\Game\Commands;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;
use BinaryStudioAcademyTests\Game\Messages;

class UnknownCommand implements Command
{
    public function execute(): string
    {
        return Messages::errors('unknown_command');
    }
}