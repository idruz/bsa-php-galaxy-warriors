<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Helpers\Random;
use BinaryStudioAcademy\Game\Contracts\Io\{Reader, Writer};
use BinaryStudioAcademy\Game\Commands\{ExitCommand, HelpCommand, StatsCommand};

class Game
{
    public $dataStorage;
    private $random;

    public function __construct(Random $random)
    {
        $this->random = $random;
        $this->dataStorage = DataStorage::getInstance();
    }

    public function start(Reader $reader, Writer $writer)
    {
        $writer->writeln('Welcome to "Galaxy Warriors".');
        $writer->writeln('Adventure has begun. Wish you good luck!');
        $writer->writeln('Press enter to start... ');

        while (true) {
            $this->run($reader, $writer);
        }
    }

    public function run(Reader $reader, Writer $writer)
    {
        $input = strtolower(trim($reader->read()));
        $output = (new Invoker())->setInputData($input);
        $writer->writeln($output);
    }
}
