<?php

namespace BinaryStudioAcademy\Game;

use BinaryStudioAcademy\Game\Contracts\Commands\Command;

class Invoker
{
    const COMMANDS_PATH = "BinaryStudioAcademy\\Game\\Commands\\";

    private $command;

    public function setInputData(string $input): string
    {
        if (strpos($input, '-') !== false) {
            $explodedData = explode('-', $input);
            $newInputData = '';
            foreach ($explodedData as $data) {
                $newInputData .= ucfirst($data);
            }
            if (!empty($newInputData)) {
                $input = $newInputData;
            }
        }

        /**
         * unknown_command
         */
        if (strpos($input, '_') !== false) {
            $explodedData = explode('_', $input);
            if (!empty($explodedData)) {
                $input = current($explodedData);
            }
        }

        $inputExploded = explode(' ', $input);
        if (
            !empty($inputExploded)
            and count($inputExploded) >= 2
        ) {
            $commandHead = $inputExploded[0];
            $commandSub = $inputExploded[1];
        }

        $commandClass = self::COMMANDS_PATH . ucfirst($commandHead ?? $input) . 'Command';
        if (class_exists($commandClass)) {
            $this->setCommand(
                new $commandClass($commandSub ?? null)
            );
            return $this->run();
        }

        return '';
    }

    public function setCommand(Command $command): void
    {
        $this->command = $command;
    }

    public function run(): string
    {
        return $this->command->execute();
    }
}